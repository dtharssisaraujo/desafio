CREATE DATABASE IF NOT EXISTS webjump;
USE webjump;
CREATE TABLE IF NOT EXISTS category(
    category_id INT NOT NULL AUTO_INCREMENT,
    code varchar(255) NOT NULL UNIQUE,
    name varchar(255) NOT NULL,
    PRIMARY KEY(category_id)
);
CREATE TABLE IF NOT EXISTS products(
    sku varchar(255) NOT NULL UNIQUE,
    name varchar(255) NOT NULL,
    price float(10),
    description varchar(255),
    amount int(255),
    category_id varchar(255) NOT NULL,
    PRIMARY KEY(sku)
);
