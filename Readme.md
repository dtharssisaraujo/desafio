# Webjump

# Overview

Test Image - Webjump

# Whats's inside

Docker images chosen for project:
    
    PHP: 7.1.10-fpm
    NGINX: latest
    mysql:5.7

# Requirements
 
    Linux operating system (debian preferably)
    Docker v17.12.0-ce or higher
    docker-compose v1.16.1 0r higher
    Git

# How-to (simple)

To start the project, simply have the docker and docker compose installed on the machine.

Run the script

    # sudo su
    # cd /pathCode/
    # ./init.sh 

After uploading all containers, you need to run a script to create the database.

    # docker exec -it dbserver mysql -r user -p password (By default the project is ready to connect with username and password -> root.)
    # source /data/application/init.sql

Finally you need to configure hosts on the Linux machine.

    # vim/ nano /etc/vhosts
    # add 172.17.0.2 webjump-dev.test:7000

Acesse (http://webjump-dev.test:7000)

