<?php 

// require __DIR__ . '/src/bootstrap.php';
error_reporting(E_ALL);
ini_set('display_errors', true);
date_default_timezone_set('America/Sao_Paulo');
session_start();
session_regenerate_id();

require_once __DIR__.'/vendor/autoload.php';

use Routes\Router;

$router = new Router();
