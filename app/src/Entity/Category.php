<?php

namespace Entity;

class Category{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $code;

    /**
     * @var int
     */
    private $codeOrigin;

    /**
     * @param string $name
     */
    public function setName(string $name) {
        $this->name = $name;
    }

    /**
     * @param int $code
     */
    public function setCode(string $code) {
        $this->code = $code;
    }

    /**
     * @param int $codeOrigin
     */
    public function setCodeOrigin(string $codeOrigin) {
        $this->codeOrigin = $codeOrigin;
    }
    
    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getCodeOrigin() {
        return $this->codeOrigin;
    }
}