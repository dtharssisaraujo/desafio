<?php

namespace Entity;

class Product{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $sku;

    /**
     * @var string
     */
    private $skuOrigin;


    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var int
     */
    private $category_id;

    /**
     * @param string $name
     */
    public function setName(string $name){
        $this->name = $name;
    }

    /**
     * @param string $sku
     */
    public function setSku(string $sku){
        $this->sku = $sku;
    }


    /**
     * @param string $skuOrigin
     */
    public function setSkuOrigin(string $skuOrigin){
        $this->skuOrigin = $skuOrigin;
    }


    /**
     * @param string $price
     */
    public function setPrice(string $price){
        $this->price = $price;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description){
        $this->description = $description;
    }

    /**
     * @param string $amount
     */
    public function setAmount(string $amount){
        $this->amount = $amount;
    }

    /**
     * @param array $category_id
     */
    public function setCategoryId($category_id){
        $this->category_id = $category_id;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSku(){
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getSkuOrigin(){
        return $this->skuOrigin;
    }

    /**
     * @return string
     */
    public function getPrice(){
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * @return string
     */
    public function getAmount(){
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCategoryId(){
        return $this->category_id;
    }
}