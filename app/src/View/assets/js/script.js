function showStuff(nameEdit, name, codeEdit, code, salvar, btn) {    
    // mostrar campo nome para editar
    document.getElementById(nameEdit).style.display = 'block';
    // ocultar span nome
    document.getElementById(name).style.display = 'none';
    // // mostrar campo code para editar
    document.getElementById(codeEdit).style.display = 'block';
    // // ocultar span code
    document.getElementById(code).style.display = 'none';
    // // ocultar botão edit
    btn.style.display = 'none';
    // // mostrar botão salvar
    document.getElementById(salvar).style.display = 'block';
}

function showStuffProducts(nameEdit, name, skuEdit, sku, priceEdit, price, amountEdit, amount, categoryEdit, category, salvar, btn) {    
    // mostrar campo nome para editar
    document.getElementById(nameEdit).style.display = 'block';
    // ocultar span nome
    document.getElementById(name).style.display = 'none';
    // // mostrar campo sku para editar
    document.getElementById(skuEdit).style.display = 'block';
    // // ocultar span sku
    document.getElementById(sku).style.display = 'none';
    // mostrar campo price para editar
    document.getElementById(priceEdit).style.display = 'block';
    // ocultar span price
    document.getElementById(price).style.display = 'none';
    // mostrar campo amount para editar
    document.getElementById(amountEdit).style.display = 'block';
    // ocultar span amount
    document.getElementById(amount).style.display = 'none';
    // mostrar campo category para editar
    document.getElementById(categoryEdit).style.display = 'block';
    // ocultar span category
    document.getElementById(category).style.display = 'none';
    document.getElementById(category).style.display = 'none';
    // ocultar botão editar
    btn.style.display = 'none';
    // mostrar botão salvar
    document.getElementById(salvar).style.display = 'block';
}

// function hideStuffProducts(cancelar){
//     // mostrar campo nome para editar
//     document.getElementById(nameEdit).style.display = 'none';
//     // ocultar span nome
//     document.getElementById(name).style.display = 'block';
//     // // mostrar campo sku para editar
//     document.getElementById(skuEdit).style.display = 'none';
//     // // ocultar span sku
//     document.getElementById(sku).style.display = 'block';
//     // mostrar campo price para editar
//     document.getElementById(priceEdit).style.display = 'nonde';
//     // ocultar span price
//     document.getElementById(price).style.display = 'block';
//     // mostrar campo amount para editar
//     document.getElementById(amountEdit).style.display = 'none';
//     // ocultar span amount
//     document.getElementById(amount).style.display = 'block';
//     // mostrar campo category para editar
//     document.getElementById(categoryEdit).style.display = 'nonde';
//     // ocultar span category
//     document.getElementById(category).style.display = 'block';
//     // mostrar botão cancelar
//     document.getElementById(cancelar).style.display = 'none';
//     // ocultar botão editar
//     btn.style.display = 'block';
//     // mostrar botão salvar
//     document.getElementById(salvar).style.display = 'nonde';
// }