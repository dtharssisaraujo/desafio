<?php

namespace Webjump\Resources\Products;

use Webjump\Resources\Products\getProducts;
use Webjump\Resources\Categories\getCategories;

$products = new getProducts();
$categories = new getCategories();

?>
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Products</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="src/View/assets/css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>

<script src="src/View/assets/js/script.js"></script>

<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="src/View/assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard"><img src="src/View/assets/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories" class="link-menu">Categorias</a></li>
      <li><a href="products" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard" class="link-logo"><img src="src/View/assets/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header --><body>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="addProduct" class="btn-action">Add new Product</a>
    </div>
    <!-- <form action="uploadfile" method="post" enctype="multipart/form-data">
        Select image to upload:
        <input type="file" name="fileToUpload" id="fileToUpload">
        <input type="submit" value="Upload Image" name="submit">
    </form> -->

    <form action="uploadfile" method="post" accept-charset="utf-8" enctype="multipart/form-data">
      <input type="file" name="file">
      <input type="submit" name="btn_submit" value="Upload File" />
    </form>


    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>

      <?php foreach ($products->getAllProducts() as $listProducts): ?>
        <form action="actionsProducts" method="POST" id="form">
          <tr class="data-row">
            <td class="data-grid-td post">
              <span id="<?php echo $listProducts["name"]; ?>" style="display: none;">
                <input type="text" name="name" value="<?php echo ucfirst($listProducts["name"]); ?>" />
              </span>

              <span id="span_<?php echo $listProducts["name"]; ?>" class="data-grid-cell-content"><?php echo $listProducts["name"]; ?></span>
            </td>
          
            <td class="data-grid-td post">
              <span id="<?php echo $listProducts["sku"]; ?>" style="display: none;">
                <input type="text" name="sku" value="<?php echo ucfirst($listProducts["sku"]); ?>" />
                <input type="hidden" name="sku-old" value="<?php echo ucfirst($listProducts["sku"]); ?>" />
              </span>

              <span id="span_<?php echo $listProducts["sku"]; ?>" class="data-grid-cell-content"><?php echo $listProducts["sku"]; ?></span>
            </td>

            <td class="data-grid-td post">
              <span id="<?php echo $listProducts["price"]; ?>" style="display: none;">
                <input type="text" name="price" value="<?php echo ucfirst($listProducts["price"]); ?>" />
              </span>

              <span id="span_<?php echo $listProducts["price"]; ?>" class="data-grid-cell-content">R$<?php echo $listProducts["price"]; ?></span>
            </td>

            <td class="data-grid-td post">
              <span id="<?php echo $listProducts["amount"]; ?>" style="display: none;">
                <input type="text" name="amount" value="<?php echo ucfirst($listProducts["amount"]); ?>" />
              </span>

              <span id="span_<?php echo $listProducts["amount"]; ?>" class="data-grid-cell-content"><?php echo $listProducts["amount"]; ?></span>
            </td>

            <td class="data-grid-td post">
              
              <span id="<?php echo $listProducts["category"]; ?>" style="display: none;">
                <div class="input-field">
                  <select multiple="multiple" name="category[]" id="category" >
                    <optgroup>
                      <?php foreach ($categories->getAllCategories() as $listCategories): ?>
                        <option selected value="<?php echo $listCategories["category_id"]; ?>"><?php echo $listCategories["name"]; ?></option>
                      <?php endforeach;?> 
                    </optgroup>
                  </select>
                </div>
              </span>
              <span id="span_<?php echo $listProducts["category"]; ?>">
                <?php foreach ($products->getCategoriesByProducts($listProducts["category"]) as $listCategoriesByProduct): ?>                
                  <span class="data-grid-cell-content"><?php echo $listCategoriesByProduct["name"]; ?> / </span>
                <?php endforeach;?> 
              </span>     
            </td>
          
            <td class="data-grid-td post">
              <div class="actions">
                <div class="action edit">
                  <span>
                    <input id="salvar_<?php echo $listProducts["sku"]; ?>" style="display: none;" class="btn-submit btn-action" type="submit" name="update" value="Salvar"/>
                    <a href="#" class="btn-submit btn-action" onclick='showStuffProducts("<?php echo $listProducts["name"]; ?>", "span_<?php echo $listProducts["name"]; ?>", "<?php echo $listProducts["sku"]; ?>", "span_<?php echo $listProducts["sku"]; ?>", "<?php echo $listProducts["price"]; ?>", "span_<?php echo $listProducts["price"]; ?>", "<?php echo $listProducts["amount"]; ?>", "span_<?php echo $listProducts["amount"]; ?>", "<?php echo $listProducts["category"]; ?>", "span_<?php echo $listProducts["category"]; ?>", "salvar_<?php echo $listProducts["sku"]; ?>", this); return false;'>Edit</a>
                  </span>
                </div>
                <div class="action delete"><span><input class="btn-submit btn-action"  type="submit" name="delete" value="Delete" /></span></div>
              </div>
            </td>
          </tr>
        </form>
      <?php endforeach;?> 
    </table>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="src/View/assets/images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
