<?php
/**
 * Classe de configuração da conexão com a base de dados
 * @author Danilo Tharssis Araujo <dtharssis@gmail.com>
 */

namespace Dbconfig;

class Client {
    /**
     * @var string
     */
    protected $serverName;

    /**
     * @var string
     */
    protected $userName;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $dbName;

    /**
     * Função com os parametros de conexão com a base de dados
     * @param string $serverName, $userName, $password, $dbName
     */
    public function Client() {
        $this->serverName = '172.20.0.1';
        $this->userName = 'root';
        $this->password = 'root';
        $this->dbName = 'webjump';
    }

    public function RemoveInfo(){
        $this->serverName = NULL;
        $this->userName = NULL;
        $this->password = NULL;
        $this->dbName = NULL;
    }
}