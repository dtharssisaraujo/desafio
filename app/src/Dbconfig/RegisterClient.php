<?php
/**
 * Classe de registro dos dados de conexão com a base de dados
 * @author Danilo Tharssis Araujo <dtharssis@gmail.com>
 */

namespace Dbconfig;

use Dbconfig\Client;

class RegisterClient extends Client {
    /**
     * @var string
     */
    protected $serverName;

    /**
     * @var string
     */
    protected $userName;

    /**
     * @var string
     */
    protected $password;
    
    /**
     * @var string
     */
    protected $dbName;

    /**
     * Função construtor para ter acesso ao dados de conexão com a base de dados
     * @param string $serverName, $userName, $password, $dbName
     */
    public function __construct() {
        try {
            $dbNew = new Client();
            $dbNew->Client();
            $this->setServerName($dbNew->serverName);
            $this->setUserName($dbNew->userName); 
            $this->setPassword($dbNew->password);
            $this->setDbName($dbNew->dbName);
            $dbNew->Removeinfo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para salvar o serverName de conexão
     * @param string $serverName
     */
    public function setServerName(string $serverName) {
        $this->serverName = $serverName;
    }

    /**
     * Função para salvar o usuário de conexão
     * @param string $user
     */
    public function setUserName(string $userName) {
        $this->userName = $userName;
    }

    /**
     * Função para salvar a senha de conexão
     * @param string $password
     */
    public function setPassword(string $password) {
        $this->password = $password;
    }

      /**
     * Função para salvar a noma da base de dados da conexão
     * @param string $password
     */
    public function setDbName(string $dbName) {
        $this->dbName = $dbName;
    }   

    /**
     * Função para salvar o serverName de conexão
     * @return string $serverName
     */
    public function getServerName() {
        return $this->serverName;
    }

    /**
     * Função para salvar o serverName de conexão
     * @return string $serverName
     */
    public function getUserName() {
        return $this->userName;
    }

    /**
     * Função para salvar o serverName de conexão
     * @return string $serverName
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Função para salvar o serverName de conexão
     * @return string $serverName
     */
    public function getDbName() {
        return $this->dbName;
    }
}