<?php
/**
 * Classe de conexão com Banco de dados
 * @author Danilo Tharssis Araujo <dtharssis@gmail.com>
 */

namespace Dbconfig;

class ConnectDb {
    /**
     * @var string
     */
    private $connection;

    /**
     * @var string
     */
    private $connPdo;

    /**
     * Função construtor de conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->connection = new RegisterClient();
            $this->connPdo =  new \PDO(
                'mysql:host=' . $this->connection->getServerName() . ';dbname=' . $this->connection->getDbName() . ';charset=utf8',
                $this->connection->getUserName(), 
                $this->connection->getPassword()
            );
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para pegar a conexão com a base de dados
     */
    public function getConnPdo(){
        return $this->connPdo;
    }

    /**
     * Função para apagar dados de conexão com a base de dados
     */
    public function disconnectConnPdo(){
        $this->connection = 'Connection';
        $this->connPdo = 'end';
        // echo $this->connection . ' ' . $this->connPdo;
    }
}