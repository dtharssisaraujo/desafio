<?php

namespace Routes;

class Router{

    /**
     * Função contrutora para executar rotas
     */
    public function __construct(){
        $this->run();
    }

    /**
     * Função para identificar o tipo de método GET ou POST
     */
    public function run () {
        $method = strtolower($_SERVER['REQUEST_METHOD']) ?? 'get';
        $route = str_replace(".html", "", $_SERVER['REQUEST_URI']) ?? '/dashboard';
        if ($method == 'get') {
            $this->get($route);
        }
        
        if ($method == 'post') {
            $this->post($route);
        }
    }

    /**
     * Função de rotas GET's
     * @param string $route
     */
    public function get(string $route){
        $whitelist = array(
            '/dashboard', '/', '/registerCategory', '/updateCategory', '/actionsCategory', '/actionsProducts', '/registerProducts'
        );        
        
        if (in_array($route, $whitelist)){
            return \Routes\Controller\AppController::index($route);
        }

        if ($route ==  '/products') {
            return \Routes\Controller\AppController::products();
        }

        if ($route ==  '/categories') {
            return \Routes\Controller\AppController::category();
        }

        if ($route ==  '/addCategory') {
            return \Routes\Controller\AppController::addCategory();
        }        

        if ($route ==  '/addProduct') {
            return \Routes\Controller\AppController::addProduct();
        }        
    }
    
    /**
     * Função de rotas POST's
     * @param string $route
     */
    public function post(string $route){
        if ($route ==  '/registerCategory') {
            return \Routes\Controller\AppController::writeCategory();
        }
        
        if ($route ==  '/updateCategory') {
            return \Routes\Controller\AppController::updateCategory();
        }
        
        if ($route ==  '/actionsCategory') {
            return \Routes\Controller\AppController::actionsCategory();
        }
        
        if ($route ==  '/registerProducts') {
            return \Routes\Controller\AppController::writeProducts();
        }

        if ($route ==  '/actionsProducts') {
            return \Routes\Controller\AppController::actionsProducts();
        }

        if ($route ==  '/uploadfile') {
            return \Routes\Controller\AppController::uploadfile();
        }
    }
}