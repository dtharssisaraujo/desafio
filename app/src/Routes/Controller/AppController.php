<?php

namespace Routes\Controller;

use Webjump\Controller\Categories\ControllerRegister as CategoryRegister;
use Webjump\Controller\Categories\ControllerUpdate as CategoryUpdate;
use Webjump\Controller\Categories\ControllerDelete as CategoryDelete;
use Webjump\Controller\Products\ControllerRegister as ProductRegister;
use Webjump\Controller\Products\ControllerUpdate as ProductUpdate;
use Webjump\Controller\Products\ControllerDelete as ProductDelete;

class AppController extends Controller {
    /**
     * Função para url atual
     */
    public static function index(string $route) {
        if ($route == "/"){
            return self::view('dashboard');
        }

        if ($route == "/actionsCategory"){
            return self::view('categories');
        }
        
        if ($route == "/updateCategory"){
            return self::view('categories');
        }
        
        if ($route == "/registerCategory"){
            return self::view('addCategory');
        }
        
        if ($route == "/registerProducts"){
            return self::view('addProduct');
        }

        if ($route == "/actionsProducts"){
            return self::view('products');
        }
        return self::view($route);
    }

    /**
     * Função para url -> lista de categorias
     */
    public static function category() {
        return self::view('categories');
    }

    /**
     * Função para url -> adicionar novas categorias
     */
    public static function addCategory() {
        return self::view('addCategory');
    }

    /**
     * Função para salvar uma nova categoria
     */
    public static function writeCategory() {
        $write = new CategoryRegister(self::params());
        self::redirect('addCategory', $write->register());
    }
 
    /**
     * Função para identificar o tipo de ação a ser tomada, seja delete ou update
     */
    public static function actionsCategory() {
        if (isset(self::params()["delete"])){
            $write = new CategoryDelete(self::params());
            self::redirect('categories', $write->delete());
        }
        
        if (isset(self::params()["update"])){
            $write = new CategoryUpdate(self::params());
            self::redirect('categories', $write->update());
        }                
    }

    /**
     * Função para url -> lista de produtos
     */
    public static function products() {
        return self::view('products');
    }

    /**
     * Função para url -> adicionar novas categorias
     */
    public static function addProduct() {
        return self::view('addProduct');
    }

    /**
     * Função para salvar uma nova categoria
     */
    public static function writeProducts() {
        $write = new ProductRegister(self::params());
        $write->save();
        self::redirect('addProduct', $write->register());
    }

     /**
     * Função para salvar uma nova categoria
     * @param string $lines
     */
    public static function writeProductsWithCsv($lines) {
        foreach ($lines as $key => $row){
            if ($key == 0) continue;
            $write = new ProductRegister(self::params($row));
            $write->saveCsv();
            $write->registerCsv();
        }        
        self::redirect('addProduct', 'ok');
    }

     /**
     * Função para identificar o tipo de ação a ser tomada, seja delete ou update
     */
    public static function actionsProducts() {
        if (isset(self::params()["delete"])){
            $write = new ProductDelete(self::params());
            self::redirect('products', $write->delete());
        }
        
        if (isset(self::params()["update"])){
            $write = new ProductUpdate(self::params());
            self::redirect('products', $write->update());
        }                
    }

    /**
     * Função para fazer upload de arquivo
     */
    public static function uploadfile() {
        $fh = fopen($_FILES['file']['tmp_name'], 'r+');
        $lines = [];
        while( ($row = fgetcsv($fh, 8000, ";")) !== FALSE ) {
            $lines[] = $row;
        }
        self::writeProductsWithCsv($lines);
        fclose($fh);
    }
}