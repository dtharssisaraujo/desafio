<?php

namespace Routes\Controller;

class Controller {
    /**
     * Função para chamar a rota solicitada 
     * @param string $_name
     */
    protected static function view(string $_name) {
        $path = $_SERVER['DOCUMENT_ROOT'] . "/src/View/assets/";
        $file = $path . "{$_name}.php";

        if(!file_exists($file))
            die("View {$file} not found!");
            
        include_once $file;
    }

    /**
     * Função para pegar os parametros do usuario
     */
    protected static function params($params = null) {
        // var_dump($_POST);
        if($params == null)
            return $_POST;
        return $params;
    }

    /**
     * Função para redirecionar a página anterior
     */
    protected static function redirect(string $to, string $return) {
        session_destroy();
        $_SESSION['return'] = $return;
        self::view($to);
        exit();
    }
}