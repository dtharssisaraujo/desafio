<?php

namespace Webjump\Resources\Products;

use Webjump\Resources\Products\listProducts;

class getProducts {
    /**
     * @var string
     */
    private $list;
 
    /**
     * @var string
     */
    private $allProducts;

    /**
     * @var string
     */
    private $totalProducts;

    /**
     * @var string
     */
    private $categoriesByProducts;

    public function __construct() {
        $this->list = new listProducts();
        $this->allProducts = $this->list->listAllProducts();
        $this->totalProducts = $this->list->listTotalProducts();
    }

    public function getAllProducts(){
        return $this->allProducts;
    }

    public function getTotalProducts(){
        return $this->totalProducts;
    }

    public function getCategoriesByProducts($categories){
        $this->categoriesByProducts = $this->list->listCategoriesByProducts($categories);        
        return $this->categoriesByProducts;
    }

}
