<?php


namespace Webjump\Resources\Products;

use Dbconfig\ConnectDb;
use Webjump\Resources\Categories\getCategories;

class updateProduct {
     /**
     * @var string
     */
    private $conn;

    /**
     * @var string
     */
    private $sqlConn;

    /**
     * @var string
     */
    private $sql;

    /**
     * Função construtor para trabalhar com a conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->conn = new ConnectDb();
            $this->sqlConn = $this->conn->getConnPdo();
            $this->conn->disconnectConnPdo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para atualizar um produtos
     * @param string
     */
    public function updateProduct($name, $sku, $price, $amount, $category_id, $skuOrigin){
        if (is_array($category_id)){
            $category_id = implode('|', $category_id);
        }        
        $sql_select = "SELECT * FROM products WHERE sku=:sku";
        $sql_update = "UPDATE products SET sku = :sku, name = :name, price = :price, amount = :amount, category_id = :category_id WHERE sku=:skuOrigin";

        $categories = [];

        try {

            if (count(explode("|", $category_id)) > 1){
                foreach (explode("|", $category_id) as $category){
                    $takeNewCategory = new getCategories();                    
                    array_push($categories, $takeNewCategory->getOneCategoryById($category)[0]['category_id']);
                }
                
            }

            $categories = implode(",", $categories);

            if (empty($categories)){
                $categories = $category_id;
            }
            
            $stmt = $this->sqlConn->prepare($sql_select);
            $stmt->execute(['sku' => $skuOrigin]);

            $product = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (!empty($product)) {
                $stmt = $this->sqlConn->prepare($sql_update);
                if ($stmt->execute(array(                    
                    ':sku' => $sku,
                    ':name' => $name,
                    ':price' => $price,
                    ':amount' => $amount,
                    ':category_id' => $categories,
                    ":skuOrigin" => $skuOrigin
                ))){
                    return ["success"];
                }
            };

            return [ "not_exists", $products];            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}