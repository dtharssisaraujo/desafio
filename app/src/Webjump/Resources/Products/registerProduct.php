<?php


namespace Webjump\Resources\Products;

use Dbconfig\ConnectDb;
use Webjump\Resources\Categories\registerCategory;
use Webjump\Resources\Categories\getCategories;

class registerProduct {
     /**
     * @var string
     */
    private $conn;

    /**
     * @var string
     */
    private $sqlConn;

    /**
     * @var string
     */
    private $sql;

    /**
     * Função construtor para trabalhar com a conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->conn = new ConnectDb();
            $this->sqlConn = $this->conn->getConnPdo();
            $this->conn->disconnectConnPdo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para salvar um novo produto
     * @param string
     */
    public function insertProduct($name, $sku, $price, $description, $amount, $category_id){
        if (is_array($category_id)){
            $category_id = implode('|', $category_id);
        }
        $sql_select = "SELECT * FROM products WHERE sku=:sku";
        $sql_insert = "INSERT INTO products (sku, name, price, description, amount, category_id) VALUES (:sku, :name, :price, :description, :amount, :category_id) ";

        $categories = [];
        try {
            if (count(explode("|", $category_id)) > 1){
                foreach (explode("|", $category_id) as $category){                    
                    $takeNewCategory = new getCategories();                    
                    array_push($categories, $takeNewCategory->getOneCategoryById($category)[0]['category_id']);
                }
                
            }

            $categories = implode(",", $categories);

            if (empty($categories)){
                $categories = $category_id;
            }            

            $stmt = $this->sqlConn->prepare($sql_select);
            $stmt->execute(['sku' => $sku]);
            
            $product = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (!$product) {                
                $stmt = $this->sqlConn->prepare($sql_insert);
                if ($stmt->execute(array(
                    ':sku' => $sku,
                    ':name' => $name,
                    ':price' => $price,
                    ':description' => $description,
                    ':amount' => $amount,
                    ':category_id' => $categories
                ))){
                    return ["success"];
                }
            }
            return [ "exists", $product];
        } catch (\Throwable $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    /**
     * Função para salvar uma lista de produtos
     * @param string
     */
    public function insertProductCsv($name, $sku, $price, $description, $amount, $category_id){
        if (is_array($category_id)){
            $category_id = implode('|', $category_id);
        }        
        $sql_select = "SELECT * FROM products WHERE sku=:sku";
        $sql_insert = "INSERT INTO products (sku, name, price, description, amount, category_id) VALUES (:sku, :name, :price, :description, :amount, :category_id) ";
        $sql_update = "UPDATE products SET sku = :sku, name = :name, price = :price, amount = :amount, category_id = :category_id WHERE sku=:sku";
        
        $categories = [];
        try {     

            if (count(explode("|", $category_id)) > 1){
                foreach (explode("|", $category_id) as $category){
                    $registerCategory = new registerCategory();
                    $registerCategory->insertCategory(ucfirst($category), strtolower($category));

                    $takeNewCategory = new getCategories();
                    array_push($categories, $takeNewCategory->getOneCategory($category)[0]['category_id']);
                }
                
            }

            $categories = implode(",", $categories);

            if (empty($categories)){
                $takeCategory = new getCategories();
                $resultCategory = $takeCategory->getOneCategory(strtolower($category_id));
                if (is_array($takeCategory->getOneCategory(strtolower($category_id)))){
                    $categories = $resultCategory = $takeCategory->getOneCategory(strtolower($category_id))[0]['category_id'];
                    // $resultCategory = implode('|', $takeCategory->getOneCategory(strtolower($category_id)));
                }else{
                    array_push($categories, $resultCategory);
                }
            }

            $stmt = $this->sqlConn->prepare($sql_select);
            $stmt->execute(['sku' => $sku]);
            
            $product = $stmt->fetch(\PDO::FETCH_ASSOC);

            
            if (!$product) {
                $stmt = $this->sqlConn->prepare($sql_insert);
            }else{
                $stmt = $this->sqlConn->prepare($sql_update);
            }             
            if ($stmt->execute(array(
                ':sku' => $sku,
                ':name' => $name,
                ':price' => $price,
                ':description' => $description,
                ':amount' => $amount,
                ':category_id' => $categories
            ))){
                return ["success"];
            }

            return [ "exists", $product];
        } catch (\Throwable $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }    
}