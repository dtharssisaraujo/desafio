<?php

namespace Webjump\Resources\Products;

use Dbconfig\ConnectDb;

class listProducts {
    /**
     * @var string
     */
    private $conn;

    /**
     * @var string
     */
    private $sql;

    /**
     * @var string
     */
    private $total;

    /**
     * Função construtor para trabalhar com a conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->conn = new ConnectDb();
            $this->sql = $this->conn->getConnPdo();
            $this->conn->disconnectConnPdo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function listAllProducts() {
        $sel_select = "SELECT
                    sku,
                    name,
                    description,
                    amount,
                    price,	
                    category_id as category
                    FROM products";
        try {
            $stmt = $this->sql->prepare($sel_select);
            $stmt->execute();
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function listTotalProducts() {
        $sql_select = "SELECT COUNT(*) as total FROM products";
        try {
            $stmt = $this->sql->prepare($sql_select);
            $stmt->execute();
            while($data = $stmt->fetch()){
                $this->total .= $data['total'];
            }
            return $this->total;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para listar todas as categorias dos produtos
     * @param string 
     */
    public function listCategoriesByProducts($categories) {
        $sql_select = "SELECT * FROM category WHERE category_id IN ($categories)";
        try {
            $stmt = $this->sql->prepare($sql_select);
            $stmt->execute([$categories]);  
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}