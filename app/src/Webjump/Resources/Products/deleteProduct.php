<?php


namespace Webjump\Resources\Products;

use Dbconfig\ConnectDb;

class deleteProduct {
     /**
     * @var string
     */
    private $conn;

    /**
     * @var string
     */
    private $sqlConn;

    /**
     * @var string
     */
    private $sql;

    /**
     * Função construtor para trabalhar com a conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->conn = new ConnectDb();
            $this->sqlConn = $this->conn->getConnPdo();
            $this->conn->disconnectConnPdo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para deletar um produto
     * @param string
     */
    public function deleteProduct($sku){
        $sql_select = "SELECT * FROM products WHERE sku=:sku";
        $sql_delete = "DELETE FROM products WHERE sku=:sku";
         try {
            $stmt = $this->sqlConn->prepare($sql_select);
            $stmt->execute(['sku' => $sku]);

            $products = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (!empty($products)) {
                $stmt = $this->sqlConn->prepare($sql_delete);
                $stmt->bindParam(':sku', $sku);
                if ($stmt->execute()){
                    return ["success"];
                }
            }

            return [ "not_exists", $products];            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}