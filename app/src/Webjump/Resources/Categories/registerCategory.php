<?php


namespace Webjump\Resources\Categories;

use Dbconfig\ConnectDb;

class registerCategory {
     /**
     * @var string
     */
    private $conn;

    /**
     * @var string
     */
    private $sqlConn;

    /**
     * @var string
     */
    private $sql;

    /**
     * Função construtor para trabalhar com a conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->conn = new ConnectDb();
            $this->sqlConn = $this->conn->getConnPdo();
            $this->conn->disconnectConnPdo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para salvar uma nova categoria
     * @param string
     */
    public function insertCategory($name, $code){
        $sql_select = "SELECT * FROM category WHERE code=:code";
        $sql_insert = "INSERT INTO category (code, name) VALUES (?, ?) ";
         try {
            $stmt = $this->sqlConn->prepare($sql_select);
            $stmt->execute(['code' => $code]);

            $categorie = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (!$categorie) {
                $stmt = $this->sqlConn->prepare($sql_insert);
                if ($stmt->execute([$code, $name])){
                    return ["success"];
                }
            }

            return [ "exists", $categorie];            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}