<?php

namespace Webjump\Resources\Categories;

use Dbconfig\ConnectDb;

class listCategories {
    /**
     * @var string
     */
    private $conn;

    /**
     * @var string
     */
    private $sql;

    /**
     * Função construtor para trabalhar com a conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->conn = new ConnectDb();
            $this->sql = $this->conn->getConnPdo();
            $this->conn->disconnectConnPdo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function listAllCategories() {
        $sql_select = "SELECT * FROM category";
        try {
            $stmt = $this->sql->prepare($sql_select);
            $stmt->execute();
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\Throwable $th) {
            throw $th;
        }
    }    

    /**
     * @param string $code
     */
    public function listOneCategory($code) {
        $sql_select = "SELECT category_id FROM category WHERE code= :code";
        try {
            $stmt = $this->sql->prepare($sql_select);
            $stmt->execute(['code' => $code]);
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\Throwable $th) {
            throw $th;
        }
    }    

    /**
     * @param string $code
     */
    public function listOneCategoryById($categories) {
        $sql_select = "SELECT category_id FROM category WHERE category_id IN ($categories)";
        try {
            $stmt = $this->sql->prepare($sql_select);
            $stmt->execute([$categories]);
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\Throwable $th) {
            throw $th;
        }
    }    
}