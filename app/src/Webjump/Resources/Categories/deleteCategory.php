<?php


namespace Webjump\Resources\Categories;

use Dbconfig\ConnectDb;

class deleteCategory {
     /**
     * @var string
     */
    private $conn;

    /**
     * @var string
     */
    private $sqlConn;

    /**
     * @var string
     */
    private $sql;

    /**
     * Função construtor para trabalhar com a conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->conn = new ConnectDb();
            $this->sqlConn = $this->conn->getConnPdo();
            $this->conn->disconnectConnPdo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para deletar uma categoria
     * @param string
     */
    public function deleteCategory($name, $code){
        $sql_select = "SELECT * FROM category WHERE code=:code";
        $sql_delete = "DELETE FROM category WHERE code=:code";
         try {
            $stmt = $this->sqlConn->prepare($sql_select);
            $stmt->execute(['code' => $code]);

            $categorie = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (!empty($categorie)) {
                $stmt = $this->sqlConn->prepare($sql_delete);
                $stmt->bindParam(':code', $code);
                if ($stmt->execute()){
                    return ["success"];
                }
            }

            return [ "not_exists", $categorie];            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}