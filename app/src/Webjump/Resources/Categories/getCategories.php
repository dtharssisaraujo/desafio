<?php

namespace Webjump\Resources\Categories;

use Webjump\Resources\Categories\listCategories;

class getCategories {
    /**
     * @var string
     */
    private $list;
 
    /**
     * @var string
     */
    private $allCategories;

    /**
     * @var string
     */
    private $oneCategory;

    /**
     * @var string
     */
    private $oneCategoryByID;

    public function __construct() {
        $this->list = new listCategories();
        $this->allCategories = $this->list->listAllCategories();        
    }

    public function getAllCategories(){
        return $this->allCategories;
    }

    public function getOneCategory($code){
        $this->oneCategory = $this->list->listOneCategory($code);
        return $this->oneCategory;
    }

    public function getOneCategoryById($categories){
        $this->oneCategoryByID = $this->list->listOneCategoryById($categories);
        return $this->oneCategoryByID;
    }
}
