<?php


namespace Webjump\Resources\Categories;

use Dbconfig\ConnectDb;

class updateCategory {
     /**
     * @var string
     */
    private $conn;

    /**
     * @var string
     */
    private $sqlConn;

    /**
     * @var string
     */
    private $sql;

    /**
     * Função construtor para trabalhar com a conexão com a base de dados
     */
    public function __construct() {
        try {
            $this->conn = new ConnectDb();
            $this->sqlConn = $this->conn->getConnPdo();
            $this->conn->disconnectConnPdo();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Função para atualizar uma categoria
     * @param string
     */
    public function updateCategory($name, $code, $codeOrigin){
        $sql_select = "SELECT * FROM category WHERE code=:code";
        $sql_update = "UPDATE category SET code = :code, name = :name WHERE code=:codeOrigin";
         try {
            $stmt = $this->sqlConn->prepare($sql_select);
            $stmt->execute(['code' => $codeOrigin]);

            $categorie = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (!empty($categorie)) {
                $stmt = $this->sqlConn->prepare($sql_update);
                if ($stmt->execute(array(
                    ":code" => $code,
                    ":name" => $name,
                    ":codeOrigin" => $codeOrigin
                ))){
                    return ["success"];
                }
            }

            return [ "not_exists", $categorie];            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}