<?php

namespace Webjump\Controller\Products;

use Entity\Product;
use Webjump\Resources\Products\updateProduct;

class ControllerUpdate {

    /**
     * @var string
     */
    private $register;

    public function __construct($params){
        $this->register = new Product();
        $this->save($params);
    }

    private function save($params){
        $this->register->setName($params["name"]);   
        $this->register->setSku($params["sku"]);
        $this->register->setSkuOrigin($params["sku-old"]);
        $this->register->setPrice($params["price"]);
        // $this->register->setDescription($params["description"]);
        $this->register->setAmount($params["amount"]);
        // var_dump($params["category_id"]);die;
        $this->register->setCategoryId($params["category"]);
    }

    public function update(){
        $updateProduct = new updateProduct();
        $result = $updateProduct->updateProduct(
            $this->register->getName(),
            $this->register->getSku(),
            $this->register->getPrice(),
            // $this->register->getDescription(),
            $this->register->getAmount(),
            $this->register->getCategoryId(),
            $this->register->getSkuOrigin()
        );

        return $result[0];
    }
}