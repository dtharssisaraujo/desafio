<?php

namespace Webjump\Controller\Products;

use Entity\Product;
use Webjump\Resources\Products\registerProduct;

class ControllerRegister {

    /**
     * @var string
     */
    private $register;

    /**
     * @var string
     */
    private $params;

    public function __construct($params){
        $this->register = new Product();
        $this->params = $params;
    }

    /**
     * Função para salvar o produto pelo admin
     */
    public function save(){
        $this->register->setName($this->params["name"]);   
        $this->register->setSku($this->params["sku"]);
        $this->register->setPrice($this->params["price"]);
        $this->register->setDescription($this->params["description"]);
        $this->register->setAmount($this->params["quantity"]);
        $this->register->setCategoryId($this->params["category"]);
    }

    /**
     * Função para salvar produtos pelo upload de arquivos
     */
    public function saveCsv(){
        $this->register->setName($this->params[0]);
        $this->register->setSku($this->params[1]);
        $this->register->setDescription($this->params[2]);
        $this->register->setAmount($this->params[3]);
        $this->register->setPrice($this->params[4]);
        $this->register->setCategoryId($this->params[5]);
    }

    public function register(){
        $registerProducts = new registerProduct();
        $result = $registerProducts->insertProduct(
            $this->register->getName(),
            $this->register->getSku(),
            $this->register->getPrice(),
            $this->register->getDescription(),
            $this->register->getAmount(),
            $this->register->getCategoryId()
        );
        
        return $result[0];
    }

    public function registerCsv(){
        $registerProducts = new registerProduct();
        $result = $registerProducts->insertProductCsv(
            $this->register->getName(),
            $this->register->getSku(),
            $this->register->getPrice(),
            $this->register->getDescription(),
            $this->register->getAmount(),
            $this->register->getCategoryId()
        );
        
        return $result[0];
    }
}