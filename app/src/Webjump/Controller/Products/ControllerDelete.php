<?php

namespace Webjump\Controller\Products;

use Entity\Product;
use Webjump\Resources\Products\deleteProduct;

class ControllerDelete {

    /**
     * @var string
     */
    private $register;

    public function __construct($params){
        $this->register = new Product();
        $this->save($params);
    }

    private function save($params){
        $this->register->setSku($params["sku"]);
    }

    public function delete(){
        $deleteProduct = new deleteProduct();
        $result = $deleteProduct->deleteProduct(
            $this->register->getSku()
        );

        return $result[0];
    }
}