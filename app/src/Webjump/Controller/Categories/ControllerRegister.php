<?php

namespace Webjump\Controller\Categories;

use Entity\Category;
use Webjump\Resources\Categories\registerCategory;

class ControllerRegister {

    /**
     * @var string
     */
    private $register;

    public function __construct($params){
        $this->register = new Category();
        $this->save($params);
    }

    private function save($params){
        $this->register->setCode($params["category-code"]);
        $this->register->setName($params["category-name"]);   
    }

    public function register(){
        $registerCategories = new registerCategory();
        $result = $registerCategories->insertCategory(
            $this->register->getName(),
            $this->register->getCode()
        );
        
        return $result[0];
    }
}