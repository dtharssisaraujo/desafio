<?php

namespace Webjump\Controller\Categories;

use Entity\Category;
use Webjump\Resources\Categories\updateCategory;

class ControllerUpdate {

    /**
     * @var string
     */
    private $register;

    public function __construct($params){
        $this->register = new Category();
        $this->save($params);
    }

    private function save($params){
        $this->register->setCodeOrigin($params["category-code-old"]);        
        $this->register->setCode($params["category-code"]);
        $this->register->setName($params["category-name"]);
    }

    public function update(){
        $updateCategory = new updateCategory();
        $result = $updateCategory->updateCategory(
            $this->register->getName(),
            $this->register->getCode(),
            $this->register->getCodeOrigin()
        );

        return $result[0];
    }
}