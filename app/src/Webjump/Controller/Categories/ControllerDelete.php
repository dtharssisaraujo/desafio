<?php

namespace Webjump\Controller\Categories;

use Entity\Category;
use Webjump\Resources\Categories\deleteCategory;

class ControllerDelete {

    /**
     * @var string
     */
    private $register;

    public function __construct($params){
        $this->register = new Category();
        $this->save($params);
    }

    private function save($params){
        $this->register->setCode($params["category-code"]);
        $this->register->setName($params["category-name"]);   
    }

    public function delete(){
        $deleteCategory = new deleteCategory();
        $result = $deleteCategory->deleteCategory(
            $this->register->getName(),
            $this->register->getCode()
        );

        return $result[0];
    }
}