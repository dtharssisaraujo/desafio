#!/usr/bin/env bash
clear

# |*****************************************|
# |         WEBJUMP                         |
# |_________________________________________|


Main () {
    echo "Build project"
    sleep 1
    cd ./build
    sleep 1
    docker-compose build    
    sleep 2
    docker-compose up -d
}

Main 